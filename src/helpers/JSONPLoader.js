/**
 * JSONP loader. CrossOrigin domains don't allow us to load requests on regular way
 */
module.exports = function(){
	var _script;
	var _callback;
	var _isBusy = false;
	
	/**
	 * Invoked when script is loaded
	 * @param {object} params 
	 */
	function onJsp(params) {
		_isBusy = false;
			//if callback is set call it
		if (_callback){
			_callback(params);
		};
			//reset callback
		_callback = null;
			//remove script tag and remove global function
		try {
			_script.parentNode.removeChild(_script);
			delete window.onJSONPcallback;
			_script = null;
		}catch(ignore){};
	}

	//PUBLIC METHODS
	/**
	 * Load JSONP file
	 * @param {string} url url that we want to load
	 * @param {function} callback function that will be invoked with response data
	 */
	function load(url, callback){
			//currently allow only one request, no parallel calls
		if (_isBusy){
			return;
		};

		_callback = callback;

			//create new <script> element to load this
		_script = document.createElement("script");
		_isBusy = true;

		document.body.appendChild(_script);
			//set global accessible function
		window.onJSONPcallback = onJsp;
		_script.src = url + "&callback=onJSONPcallback";

	}

	return Object.freeze({
		load: load
	});
};