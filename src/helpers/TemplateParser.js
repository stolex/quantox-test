var mustache = require("mustache");

/**
 * (Singleton). Loads template files, cache them, and use Mustache to parse them. 
 */
var TemplateParser = function(){
		//cached template files
	var _loadedTemplates = [];
		//Ajax loader
	var _templateLoader;
		//returned object
	var _response;
		//all callbacks that are waiting for parsed template
	var _asyncListeners = [];

		//Singleton instance
	if (TemplateParser.$instance) {
		return TemplateParser.$instance;
	}

	/**
	 * If we have template for this url cached, returns it. If not return empty string
	 * @param {string} templateUrl 
	 */
	function getTemplate(templateUrl) {
		var i;
		for (i = 0; i < _loadedTemplates.length; i++) {
			if (_loadedTemplates[i].url == templateUrl) {
				return _loadedTemplates[i].data;
			};
		};
		return "";
	}

	/**
	 * Ajax loader for template file
	 * @param {string} url 
	 */
	function loadTemplate(url) {
		var i;
		var c_asyncLoader;

		if (!_templateLoader) {
			_templateLoader = new XMLHttpRequest();
		}
		_templateLoader.onreadystatechange = (function () {
			if (_templateLoader.readyState == 4 && _templateLoader.status == 200) {
					//when template is loaded cache it
				_loadedTemplates.push({
					url: url,
					data: _templateLoader.responseText
				});
					//check all async methods that are waiting for response
				for (i=0; i<_asyncListeners.length; i++){
					if (_asyncListeners[i].templateUrl === url){
						c_asyncLoader = _asyncListeners.splice(i, 1)[0];
						if (typeof(c_asyncLoader.callback) === "function"){
							c_asyncLoader.callback(mustache.render(_templateLoader.responseText, c_asyncLoader.vars));
						};
					}
				};
			}
		});
		_templateLoader.open("GET", url, true);
		_templateLoader.send();
	}

	//PUBLIC
	/**
	 * Called to use template (from url), with variables. If template is cached callback will be called imediatelly, if not, will be called after fetching data.
	 * @param {string} templateUrl 	template url that need to be loaded
	 * @param {object} vars 	variables in object that will be used in template
	 * @param {function} callback function that will be called when template is ready and parsed
	 */
	function renderTemplate(templateUrl, vars, callback) {
		var c_template = getTemplate(templateUrl);
		_vars = vars;

		if (!c_template) {
				//if we don't have template in cache, save this parameters to be invoked when template is loaded
			_asyncListeners.push({
				templateUrl: templateUrl,
				vars: vars,
				callback: callback
			})
			loadTemplate(templateUrl);
			return;
		}
			//if we have loaded template, call callback function imediatelly
		callback(mustache.render(c_template, vars));
	}

	_response = {
		renderTemplate: renderTemplate
	};
	TemplateParser.$instance = _response;

	return _response;
}

module.exports = TemplateParser;