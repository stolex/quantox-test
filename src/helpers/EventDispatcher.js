/**
 * Simplified event dispatcher
 * @param {Object} scope 
 */
module.exports = function (scope) {

		//all subscribed events stored in this hash map. keys are event names
	var _listeners = {};
	if (typeof(scope) === "undefined"){
		scope = null;
	}

	/**
	 * Fire event, call all functions subscribed to this event. All parameters after event name
	 * will be used in callback call
	 * @param {string} eventName 
	 */
	function fireEvent(eventName){
		var c_listeners;
		var c_arguments = null;
			//check if fireEvents are called with more parameters, and get those parameters as Array
		if (arguments && arguments.length > 1){
			c_arguments = Array.prototype.slice.apply(arguments, [1]);
		};

			//search for methods for this eventName
		if (_listeners.hasOwnProperty(eventName)){
			c_listeners = _listeners[eventName];
				//call each function in selected scope, and pass all parameters
			c_listeners.forEach(function(func){
				if (typeof(func) === "function"){
					func.apply(scope, c_arguments);
				}
			})
		}
	}

	/**
	 * Subscribe to listen event changes
	 * @param {string} event name of event that listens
	 * @param {function} func callback function that will be invoked
	 */
	function subscribe(event, func) {
		if (!event){
			return;
		};
			//if we don't have functions for this event create new Array to store them
		if (!_listeners[event]){
			_listeners[event] = [];
		};
		_listeners[event].push(func);
	}

	/**
	 * Unregister for listening events
	 * @param {string} event Name of event to unsubscribe
	 * @param {function} func function that we want to unregister
	 */
	function unsubscribe(event, func) {
		var c_functions;
		var i;

		if (!event) {
			return;
		};
		if (_listeners[event]) {
			c_functions = _listeners[event];
				//search for function in list of functions and remove it
			for (i=0; i<c_functions.length; i++){
				if (c_functions[i] == func){
					c_functions.splice(i, 1);
					return;
				}
			};
		};
	}

	return Object.freeze({
		subscribe: subscribe,
		unsubscribe: unsubscribe,
		fireEvent: fireEvent
	});
}