/**
 * Singleton main model object
 */
var Data = function(){
	var _response;
	var _APIhost;

	if (Data.$instance){
		return Data.$instance;
	}

		//private macro for API request
	_APIhost = "https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json?lat={{latitude}}&lng={{longitude}}&fDstL=0&fDstU=100&trFmt=sa&ldv={{lastDv}}";

	//PUBLIC FUNCTIONS
	/**
	 * Generate new API request (with only new data)
	 * @param {string} lastDv 
	 */
	function getAPIurl(lastDv){
		if (!lastDv){
			lastDv = "";
		};
		if (_response.latitude && _response.longitude){
			return _APIhost.replace("{{latitude}}", _response.latitude)
						.replace("{{longitude}}", _response.longitude)
						.replace("{{lastDv}}", lastDv);
		};
		return "";
	}
	
	_response = {
		latitude: null,
		longitude: null,
		getAPIurl: getAPIurl,
			//last response with all airplanes
		airplanesList: []
	};
	Data.$instance = _response;

	return _response;
}

module.exports = Data;