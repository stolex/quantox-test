var ViewBase = require("./ViewBase");

/**
 * Page that displays error when Geodata isn't available
 * @param {DOM} view 
 */
var GeolocationProblem = function (view) {
	var _dom;
	var _message;

	/**
	 * Based of problem number set text
	 * @param {Number} problem 
	 */
	function setProblem(problem) {
		if (problem == GeolocationProblem.GEOLOCATION_NOT_SUPPORTED){
			_message = "Your browser don't support geolocation.";
		}else{
			_message = "Application can't work until you share geolocation.";
		};
		this.renderTemplate("templates/geo-problem.html", { message: _message });
	};

	(function () {
		_dom = document.createElement("div");
		view.appendChild(_dom);
	})();


	return Object.assign({
		setProblem: setProblem,
	}, new ViewBase({
		container: view,
		view: _dom
	}));

};

//static "constants" for problem
GeolocationProblem.GEOLOCATION_NOT_SUPPORTED = 0;
GeolocationProblem.GEOLOCATION_REJECTED = 1;

module.exports = GeolocationProblem;