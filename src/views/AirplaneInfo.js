var ViewBase = require("./ViewBase");

/**
 * Show page with selected airplane data
 * @param {DOM} view 
 */
module.exports = function(view){
		//this page view DOM object
	var _dom;
		//airplane data (getter/setter)
	var _data;
	
		//initialization
	(function(){
		_dom = document.createElement("div");
		view.appendChild(_dom);
	})();
	
	return Object.assign({
		get data(){
			return _data;
		},
		set data(value){
			_data = value;
				//based on received data we will call template to parse output
			this.renderTemplate("templates/airplane-info.html", _data);
		},
		set logo(value){
			var c_image;
			c_image = view.getElementsByClassName("logo");
			if (c_image && c_image.length > 0){
				c_image = c_image[0];
				c_image.src = value;
			};
		}
	}, new ViewBase({
		container: view,
		view: _dom
	}));
}