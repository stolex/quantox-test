var TemplateParser = require("../helpers/TemplateParser");

/**
 * Base class that will be used as parent for composition. Contains methods that all View modules need to have (show, hide and template)
 * @param {object} scope 
 */
module.exports = function(scope){
		//module that parses templates with provided variables
	var _templateParser = new TemplateParser();
	
	/**
	 * Add current view to main container
	 */
	function show(){
		scope.container.appendChild(scope.view);
	}

	/**
	 * Hide/remove current DOM object from parent container
	 */
	function hide(){
		scope.container.removeChild(scope.view);
	}

	/**
	 * Use template parser to generate output for provided vars
	 * @param {string} templateUrl 
	 * @param {object} vars 
	 * @param {function} callback Optional, called when dom is updated
	 */
	function renderTemplate(templateUrl, vars, callback){
		_templateParser.renderTemplate(templateUrl, vars, function(data){
			scope.view.innerHTML = data;
			if (callback){
				callback();
			};
		});
	}

	return {
		show: show,
		hide: hide,
		renderTemplate: renderTemplate
	}
}