var ViewBase = require("./ViewBase");

/**
 * List of airplanes view
 * @param {DOM} view 
 */
module.exports = function(view){
	var _flights;
	var _dom;
	var _listBox;

	(function(){
		_dom = document.createElement("div");
		view.appendChild(_dom);
	})();

	return Object.freeze(
		Object.assign({
			get flights(){
				return _flights;
			},
			set flights(data){
					//sort data by altitude
				_flights = data.sort(function(a,b){
					if (a.altitude && b.altitude){
						if (a.altitude > b.altitude){
							return -1;
						}else if (a.altitude < b.altitude) {
							return 1;
						}
					};
					return 0
				});
				this.renderTemplate("templates/airplane-list.html", {flights:_flights});
			}
		}, new ViewBase({
			container: view,
			view: _dom
		}))
	);
};