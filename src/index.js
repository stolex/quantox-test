var Controller = require("./controllers/HomePageController");
var Polyfils = require("./helpers/Polyfils");

(function(){
		//main application div container
	var _container;
		//main app controller
	var _controller;

		//call to implement polyfils
	new Polyfils();

		//when page is loaded and dom elements are ready
	function onDomReady(){
		window.removeEventListener("load", onDomReady);
			//get container
		_container = document.getElementById("container");

			//controller initialization
		new Controller(_container);
	}

	window.addEventListener("load", onDomReady);
})();