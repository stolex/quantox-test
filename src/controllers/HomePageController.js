var AirplanesListController = require("../controllers/AirplanesListController");
var AirplanesList = require("../views/AirplanesList");
var GeolocationProblem = require("../views/GeolocationProblem");
var PageRouter = require("../controllers/PageRouter");
var AirplaneInfo = require("../views/AirplaneInfo");
var AirplaneInfoController = require("../controllers/AirplaneInfoController");
var Data = require("../model/Data");

/**
 * Main controller. Get main DOM instance, initialize router, switch between views
 * @param {DOM} view 
 */
module.exports = function(view){
		//main View container
	var _view = view;
		//global data
	var _data = new Data();
		//saved all already initialized pages
	var _initializedPages = {};
		//current page object (view + controller)
	var _currentPage;
		//router intance
	var _router;

	/**
	 * Invoke request from browser for geolocation
	 */
	function getLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(onGeolocationReceived, onGeolocationDenied);
		} else {
			showGeolocation(GeolocationProblem.GEOLOCATION_NOT_SUPPORTED);
		}
	}

	/**
	 * Callback when user accepted Geolocation sharing
	 * @param {object} position 
	 */
	function onGeolocationReceived(position){
		_data.latitude = position.coords.latitude;
		_data.longitude = position.coords.longitude;
		showAirplanesList();
	}

	/**
	 * callback when user denied to share location
	 */
	function onGeolocationDenied(){
		showGeolocation(GeolocationProblem.GEOLOCATION_REJECTED);
	}
	
	/**
	 * Show Geolocation page, where user is informed that something is wrong with getting geolocation
	 * @param {number} problem  - Number(int) value of reason for this problem GeolocationProblem.GEOLOCATION_REJECTED or GeolocationProblem.GEOLOCATION_NOT_SUPPORTED
	 */
	function showGeolocation(problem){
		var c_view;
		var c_page;

		hideCurrentView();
			//check if this page is already initialized or create new
		if (_initializedPages.hasOwnProperty("geoProblem")){
			c_page = _initializedPages.geoProblem;
			c_view = c_page.view;
			c_view.show();
		}else{
			c_view = new GeolocationProblem(_view);
			_initializedPages.geoProblem = {
				view: c_view
			};
		}
			//set view parameter why we have problem
		c_view.setProblem(problem);
		_currentPage = c_page;
	}
	
	/**
	 * initialize Page to show list of airplanes in range
	 */
	function showAirplanesList(){
		var c_view;
		var c_page;
		hideCurrentView();

		if (_initializedPages.hasOwnProperty("airplanesList")) {
			c_page = _initializedPages.airplanesList;
			c_view = c_page.view;
			c_view.show();
		} else {
			c_view = new AirplanesList(_view);
			c_page = {
				view: c_view,
				controller: new AirplanesListController(c_view)
			};
			_initializedPages.airplanesList = c_page;
		}
		_currentPage = c_page;
	}

	/**
	 * Page to show info about flight
	 * @param {Number} id id from response list
	 */
	function showAirplaneInfo(id){
		var c_page;
		var c_view;
		var c_controller;
		
		hideCurrentView();

		if (_initializedPages.hasOwnProperty("airplaneInfo")) {
			c_page = _initializedPages.airplaneInfo;
			c_page.view.show();
			c_controller = c_page.controller;
		} else {
			c_view = new AirplaneInfo(_view);
			c_controller = new AirplaneInfoController(c_view);
			c_page = {
				view: c_view,
				controller: c_controller
			};
			_initializedPages.airplaneInfo = c_page;
		}
		c_controller.id = id;
		_currentPage = c_page;
	}

	/**
	 * hide (remove from screen) current page, usually before we need to initialize new page
	 */
	function hideCurrentView(){
		if (_currentPage){
			_currentPage.view.hide();
		};
	}

	/**
	 * Invoked when user changed hash in url location (new page request)
	 * @param {string} page 
	 */
	function onNavigationChanged(page){
		var c_temp;
		if (page == ""){
				//default page is Airplanes list
			if (_data.latitude && _data.longitude){
				showAirplanesList();
			}else{
				//TODO: probably some geolocation problem page
			}
		}else if (page.indexOf("info") == 0){
				//for info page split pageName with "-" and use second parameter as id
			c_temp = page.split("-");
			if (c_temp.length > 1){
				showAirplaneInfo(c_temp[1]);
			};
		}
	}
	
		//our small router initialization
	_router = new PageRouter();
	_router.subscribe(PageRouter.ON_NAVIGATION_CHANGED, onNavigationChanged);
	
		//start geolocation request, required parameter for this
	getLocation();
	
	if (!_view){
		throw new Error("View DOM element is not set");
	};

};