var Data = require("../model/Data");
var JSONPloader = require("../helpers/JSONPLoader");

/**
 * Controller for main screen, with list of all flights in selected radius
 * @param {DOM} view 
 */
module.exports = function(view){
		//view DOM element that this controller will controll
	var _view = view;
		//global data
	var _data = new Data();
		//how often we will check for updated data
	var REFRESH_TIME = 60000;
		//loader that we will use for getting airplanes data
	var _apiLoader;
		//API use this parameter to send only updated flights in next request
	var _lastDv;
		//our filtered list of all airplanes (without unused data)
	var _airplanes;
		//Interval instance that reload API request
	var _loadingItervals;

	/**
	 * Received data from API call
	 * @param {Object} data JSON formated object with response
	 */
	function onAPIdata(data){
		var c_airplanes;
		var c_data;
		var c_responseTimestamp;
		var i;

		if (data){
			_airplanes = [];

				//update lastDv parameter that we will use in next API call
			if (data.lastDv){
				_lastDv = data.lastDv;
			};
			if (data.acList){
				c_airplanes = data.acList;
					//if we asked only for updated list, update current list with new data
				if (_data.airplanesList && _data.airplanesList.length > 0){
						//for each new record find old and update it. If old is not found, save this record as new (push)
					c_airplanes.forEach(function(airplane){
						var i;
						for (i=0; i<_data.airplanesList.length; i++){
							if (airplane.Id == _data.airplanesList[i].Id){
								_data.airplanesList[i] = airplane;
								return;
							}
						}
						_data.airplanesList.push(airplane);
					});
						//update local array with full updated list
					c_airplanes = _data.airplanesList;

						//if API returned timestamp of response
					if (data.stm){
							//if airplane is not refreshed in last 5 updates, remove that airplane
						c_responseTimestamp = data.stm - 5 * REFRESH_TIME;
						for (i=0; i<_data.airplanesList.length; i++){
							if (_data.airplanesList[i].PosTime < c_responseTimestamp){
								_data.airplanesList.splice(i, 1);
								--i;
							}
						}
					}
				}else{
						//for first time, just use retrieved list
					_data.airplanesList = c_airplanes;
				};

					//now prepare reponse data
				c_airplanes.forEach(function(airplane){
					c_data = {
						id: airplane.Id,
						altitude: airplane.GAlt,
						operater: airplane.Op,
						model: "",
						reg: airplane.Reg
					};
						//in the case that Mdl is not set, or Mdl don't contains Manufacturer
					if (airplane.hasOwnProperty("Mdl")) {
						if (airplane.hasOwnProperty("Man") && airplane.Mdl.indexOf(airplane.Man) === -1) {
							c_data.model = airplane.Man + " " + airplane.Mdl;
						} else {
							c_data.model = airplane.Mdl;
						};
					}

						//on each 4 records, second is longitude. First we check do we have more than 2 longitude records
					if (airplane.Cos && airplane.Cos.length > 8){
							//now get diff between first and last latitude record
						c_data.direction = airplane.Cos[1] - airplane.Cos[(Math.floor(airplane.Cos.length / 4) - 1)*4 + 1] > 0;
					};
					_airplanes.push(c_data);
				});
					//update View with new data
				_view.flights = _airplanes;
			}
		}
	};

	/**
	 * initialize JSONP loader and call new load of API data
	 */
	function loadAPIdata(){
		if (!_apiLoader){
			_apiLoader = new JSONPloader();
		};
		_apiLoader.load(_data.getAPIurl(_lastDv), onAPIdata);
	};

		//start refreshing API
	loadAPIdata();
	_loadingItervals = setInterval(loadAPIdata, REFRESH_TIME);
	
	return Object.freeze({
		get view(){
			return _view
		}
	});

};