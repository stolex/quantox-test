var Data = require("../model/Data");

/**
 * Controller for Info page.
 * For selected airplane ID check full data and prepare object with data only used by view.
 * Also check for airplane operator and ask 3rd party service to get logo
 * @param {DOM} view 
 */
module.exports = function (view) {
	//global data
	var _data = new Data();
	//id that user selected
	var _airplaneId;
	//Ajax loader for 3rd party service Clearbit
	var _clearBitLoader;

	/**
	 * For selected operator call ClearBit to get list of possible logo images
	 * @param {string} opName 
	 */
	function getLogo(opName) {
		if (!opName) {
			return;
		};

		if (!_clearBitLoader) {
			if (window.XMLHttpRequest) {
				// code for modern browsers
				_clearBitLoader = new XMLHttpRequest();
			} else {
				// code for old IE browsers
				_clearBitLoader = new ActiveXObject("Microsoft.XMLHTTP");
			}
			_clearBitLoader.onreadystatechange = (function () {
				var c_response;
				if (_clearBitLoader.readyState == 4 && _clearBitLoader.status == 200) {
					try {
						//parse clearbit response
						c_response = JSON.parse(_clearBitLoader.responseText);
						//if we have at least 1 company for this search results get first one
						if (c_response && c_response.length > 0) {
							if (c_response[0].logo) {
								view.logo = c_response[0].logo;
							}
						}
					} catch (ignore) { };
				}
			}).bind(this);
		};

		//clearBit replace spaces in query with "+"
		opName = opName.replace(/(\s{1,})/ig, "+");

		try {
			_clearBitLoader.open("GET", "https://autocomplete.clearbit.com/v1/companies/suggest?query=" + opName, true);
			_clearBitLoader.send();
		} catch (ignore) { };
	}

	/**
	 * Check all aiplanes in global data, to find what user selected
	 */
	function updateInfo() {
		var i;
		var c_airplanes;
		var c_airplaneInfo;
		var c_viewData;

		if (_data.airplanesList) {
			c_airplanes = _data.airplanesList;
			for (i = 0; i < c_airplanes.length; i++) {
				//search by id
				if (c_airplanes[i].Id == _airplaneId) {
					c_airplaneInfo = c_airplanes[i];
					//prepare only data that we need in View
					c_viewData = {};
					c_viewData.from = c_airplaneInfo.From ? c_airplaneInfo.From : "";
					c_viewData.to = c_airplaneInfo.To ? c_airplaneInfo.To : "";
					c_viewData.model = "";
					//it is possible that Mdl is not set
					if (c_airplaneInfo.hasOwnProperty("Mdl")) {
						//sometimes Manufactured is part of Model string. If it is not mix them
						if (c_airplaneInfo.hasOwnProperty("Man") && c_airplaneInfo.Mdl.indexOf(c_airplaneInfo.Man) === -1) {
							c_viewData.model = c_airplaneInfo.Man + " " + c_airplaneInfo.Mdl;
						} else {
							c_viewData.model = c_airplaneInfo.Mdl;
						};
					};
					//set view data
					view.data = c_viewData;
					//call search enging for logos
					getLogo(c_airplaneInfo.Op);
					return;
				}
			};
		};
	};

	//return public accessible methods/properties
	return Object.freeze({
		get id() {
			return _airplaneId;
		},
		set id(value) {
			_airplaneId = value;
			updateInfo();
		}
	})

}