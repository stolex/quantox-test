var EventDispatcher = require("../helpers/EventDispatcher")

/**
 * Small custom page router
 */
var PageRouter = function () {
		//public functions that will be returned
	var _response = {};

		//simple Event dispatcher implementation
	var _dispatcher = new EventDispatcher(_response);

	/**
	 * check current url tag and dispatch parsed page name
	 */
	function checkUrl(){
		var c_hash = window.location.hash;
		
			//at least one character is imporant
		if (c_hash.length > 1){
				//remove # from page name
			if (c_hash.charAt(0) === "#"){
				c_hash = c_hash.slice(1);
			}
		}else{
				//no hash
			c_hash = "";
		}
			//dispatch new page event
		_dispatcher.fireEvent(PageRouter.ON_NAVIGATION_CHANGED, c_hash);
	}

		//listen page window for hash changes, and call our function to parse page
	window.addEventListener("hashchange", checkUrl);

		//prepare response object with event dispatcher methods
	_response.subscribe = _dispatcher.subscribe;
	_response.unsubscribe = _dispatcher.unsubscribe;

	return Object.freeze(_response);
}

	//static constant for changed events
PageRouter.ON_NAVIGATION_CHANGED = "changed";

module.exports = PageRouter;