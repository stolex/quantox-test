var gulp = require("gulp");
var browserify = require("gulp-browserify");
var browserSync = require("browser-sync");

const DESTINATION = "./build/";
const SOURCE = "./src/";

gulp.task("js", function () {
	gulp.src(SOURCE + "index.js")
		.pipe(browserify({
			insertGlobals: true,
			debug: !gulp.env.production
		}))
		.pipe(gulp.dest(DESTINATION));
});

gulp.task("html", function () {
	return gulp.src(SOURCE + "**/*.html")
		.pipe(gulp.dest(DESTINATION));
});

gulp.task("static", function () {
	return gulp.src(SOURCE + "static/**/*")
		.pipe(gulp.dest(DESTINATION + "static/"));
});

gulp.task("watch", function () {
	gulp.watch(SOURCE + "**/*.html", ["html"]);
	gulp.watch(SOURCE + "**/*.js", ["js"]);
	gulp.watch(SOURCE + "static/**/*", ["static"]);
});

//autorefresh server
gulp.task("serve", ["html", "js", "static", "watch"], function () {
	browserSync.init(null, {
		server: {
			baseDir: DESTINATION,
			directory: false,
			index: "index.html"
		},
		debugInfo: false,
		open: false
	}, function (err, bs) { });
	gulp.watch(DESTINATION + "**/*.*", browserSync.reload);
});

gulp.task("default", ["html", "js", "static"]);